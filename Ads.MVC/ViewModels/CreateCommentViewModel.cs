﻿namespace Ads.MVC.ViewModels
{
    public class CreateCommentViewModel
    {
        public string Comment { get; set; }
    }
}